#Love Letter
#Game Description


All of the eligible young men (and many of the not-so-young) seek to woo the princess of Tempest. Unfortunately, she has locked herself in the palace, and you must rely on others to take your romantic letters to her. Will yours reach her first?

Love Letter is a game of risk, deduction, and luck for 2–4 players. Your goal is to get your love letter into Princess Annette's hands while deflecting the letters from competing suitors. From a deck with only sixteen cards, each player starts with only one card in hand; one card is removed from play. On a turn, you draw one card, and play one card, trying to expose others and knock them from the game. Powerful cards lead to early gains, but make you a target. Rely on weaker cards for too long, however, and your letter may be tossed in the fire!

Number 4 in the Tempest Shared World Game Series

- - -

## Protocol for Issues.

Simply take an issue and assign yourself as an assignee so all other programmers know you are working on it. The same goes that if you see another programmers name on an issue you will not attempt to do their work for them without their explicit permission.

### Creating issues

Issues can be created by anyone, please try to keep them at as much of a base level as possible (within reason) do not assign these issues to another coder without explicit permission and be sure to keep the explanation for it as simple and clear as possible.