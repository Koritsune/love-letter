﻿using LoveLetterController.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LoveLetterWPF.Menus.CustomControls
{
    /// <summary>
    /// Interaction logic for EnemyPlayer.xaml
    /// </summary>
    public partial class EnemyPlayer : UserControl
    {
        Hand hand = new Hand();
        GameRoomPlayer playerIcon;

        public EnemyPlayer(Player player)
        {
            InitializeComponent();
            canvas.Children.Add(hand);
            Hand testHand = new Hand();
            canvas.Children.Add(testHand);
            hand.addCard(new PlayingCard(GameConstants.CARDS.Baron, true));
            hand.addCard(new PlayingCard(GameConstants.CARDS.Princess, false));
            hand.Margin = new Thickness(0, 150, 0, 0);
            hand.IsEnabled = false;
            playerIcon = new GameRoomPlayer(player, false);
            playerIcon.Margin = new Thickness(-32, -5, 0, 0);
            playerIcon.Height = 155;
            canvas.Children.Add(playerIcon);
        }
    }
}
