﻿using LoveLetterController.Models;
using LoveLetterWPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LoveLetterWPF.Menus.CustomControls
{
    /// <summary>
    /// Interaction logic for WaitingRoomPlayer.xaml
    /// </summary>
    public partial class GameRoomPlayer : UserControl
    {
        public Player Player;
        static SolidColorBrush greenBrush = new SolidColorBrush(Colors.Green);
        static SolidColorBrush yellowBrush = new SolidColorBrush(Colors.Yellow);
        static SolidColorBrush redBrush = new SolidColorBrush(Colors.Red);

        public GameRoomPlayer(Player _player, bool isHost)
        {
            InitializeComponent();

            Player = _player;
            Player.ConnectionStatusChanged += Player_ConnectionStatusChanged;
            player_ConnectionStatusChanged();

            LBLUsername.Content = Player.Username;

            ImgPlayer.Source = EnvironmentInformation.IconPictures[Player.DisplayIndex];

            if (isHost && Player.Username != EnvironmentInformation.Username)
            {
                this.MouseEnter += WaitingRoomPlayer_MouseEnter;
                this.MouseLeave += WaitingRoomPlayer_MouseLeave;

                BTNKick.Click += BTNKick_Click;
            }
        }

        void Player_ConnectionStatusChanged(Player player)
        {
            LBLUsername.Dispatcher.Invoke(new ThreadStart(() => player_ConnectionStatusChanged()));
        }

        void player_ConnectionStatusChanged()
        {
            if (Player.Connection_Status == Player.PlayerStatus.GOOD)
            {
                StatusIcon.Fill = greenBrush;
            }
            else if (Player.Connection_Status == Player.PlayerStatus.WARNING)
            {
                StatusIcon.Fill = yellowBrush;
            }
            else
            {
                StatusIcon.Fill = redBrush;
            }
        }

        void BTNKick_Click(object sender, RoutedEventArgs e)
        {
            Player.Remove();
        }

        void WaitingRoomPlayer_MouseLeave(object sender, MouseEventArgs e)
        {
            BTNKick.Visibility = System.Windows.Visibility.Hidden;
        }

        void WaitingRoomPlayer_MouseEnter(object sender, MouseEventArgs e)
        {
            BTNKick.Visibility = System.Windows.Visibility.Visible;
        }
    }
}
