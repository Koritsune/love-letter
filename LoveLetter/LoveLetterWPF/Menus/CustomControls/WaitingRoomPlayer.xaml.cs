﻿using LoveLetterController.Models;
using LoveLetterWPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LoveLetterWPF.Menus.CustomControls
{
    /// <summary>
    /// Interaction logic for WaitingRoomPlayer.xaml
    /// </summary>
    public partial class WaitingRoomPlayer : UserControl
    {
        public Player Player;
        static SolidColorBrush greenBrush = new SolidColorBrush(Colors.Green);
        static SolidColorBrush grayBrush = new SolidColorBrush(Colors.Gray);

        public WaitingRoomPlayer(Player _player, bool isHost)
        {
            InitializeComponent();

            Player = _player;
            Player.ReadyStatusChanged += player_ReadyStatusChanged_Invoke;

            LBLUsername.Content = Player.Username;

            ImgPlayer.Source = EnvironmentInformation.IconPictures[Player.DisplayIndex];

            player_ReadyStatusChanged();

            if (isHost && Player.Username != EnvironmentInformation.Username)
            {
                this.MouseEnter += WaitingRoomPlayer_MouseEnter;
                this.MouseLeave += WaitingRoomPlayer_MouseLeave;

                BTNKick.Click += BTNKick_Click;
            }
        }

        void BTNKick_Click(object sender, RoutedEventArgs e)
        {
            Player.Remove();
        }

        void WaitingRoomPlayer_MouseLeave(object sender, MouseEventArgs e)
        {
            BTNKick.Visibility = System.Windows.Visibility.Hidden;
        }

        void WaitingRoomPlayer_MouseEnter(object sender, MouseEventArgs e)
        {
            BTNKick.Visibility = System.Windows.Visibility.Visible;
        }

        void player_ReadyStatusChanged_Invoke()
        {
            LBLUsername.Dispatcher.Invoke(new ThreadStart(() => player_ReadyStatusChanged()));
        }

        void player_ReadyStatusChanged()
        {
            if (Player.Ready)
            {
                StatusIcon.Fill = greenBrush;
            }
            else
            {
                StatusIcon.Fill = grayBrush;
            }
        }
    }
}
