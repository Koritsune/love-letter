﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LoveLetterWPF.Controllers;
using System.Threading;
using LoveLetterWPF.Menus.CustomControls;
using LoveLetterWPF.Models;
using LoveLetterController.Models;
using LoveLetterController.Controllers;
using LoveLetterWPF.Dialogs;
using MahApps.Metro.Controls.Dialogs;

namespace LoveLetterWPF.Menus
{
    /// <summary>
    /// Interaction logic for WaitingRoom.xaml
    /// </summary>
    public partial class WaitingRoom : UserControl
    {
        public double windowHeight = 660;
        public double windowWidth = 955;

        MainWindow mainWindow; //needed for timeout and date

        public delegate void buttonClick();

        public buttonClick BTNCloseClick;

        NetworkController networkController;

        List<WaitingRoomPlayer> playerDisplays = new List<WaitingRoomPlayer>();

        readonly Thickness[] displayLocations = { new Thickness(25, 3, 0, 0), new Thickness(175, 3, 0, 0), new Thickness(25,163,0,0), new Thickness(175,163,0,0) };
        const double PlayerIconWidth = 150;
        const double PlayerIconHeight = 150;

        DialogTimeoutSelection dialogTimeoutSelection = new DialogTimeoutSelection();
        DialogLastDate dialogLastDate = new DialogLastDate();

        public WaitingRoom(MainWindow _mainWindow)
        {
            InitializeComponent();

            mainWindow = _mainWindow;
            networkController = MainWindow._networkController;

            BTNMainMenu.Click += BTNClose_Click;

            BTNSend.Click += BTNSend_Click;

            BTNReady.Click += BTNReady_Click;
            BTNNotReady.Click += BTNNotReady_Click;
            BTNTimeout.Click += BTNTimeout_Click;
            BTNDate.Click += BTNDate_Click;
            BTNStart.Click += BTNStart_Click;

            TXTTextToSend.PreviewKeyDown += TXTTextToSend_PreviewKeyDown;
            TXTTextToSend.Loaded += TXTTextToSend_Loaded;

            GameController.GameBoardCreated += GameController_GameBoardCreated;
            GameController.GameBoard.NewPlayerAdded += newPlayer;
            GameController.GameBoard.PlayerRemoved += gameBoard_PlayerRemoved;

            networkController.ChatPacketReceived += networkController_ChatPacketReceived;
            networkController.PublicIPFound += networkController_PublicIPFound;

            LBLNetworkInformation.Loaded += LBLNetworkInformation_Loaded;

            dialogTimeoutSelection.CloseEvent += dialogTimeoutSelection_CloseEvent;
            dialogLastDate.CloseEvent += dialogLastDate_CloseEvent;
        }

        async void BTNStart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (GameController.canGameStart())
                {
                    GameController.sendGameStartPacket();
                }
            }
            catch (GameBoard.GameStartException e2)
            {
                mainWindow.ShowMessageAsync("Game start error", e2.Reason);
            }
        }

        async void BTNDate_Click(object sender, RoutedEventArgs e)
        {
            await mainWindow.ShowMetroDialogAsync(dialogLastDate);        
        }

        async void dialogLastDate_CloseEvent()
        {
            await mainWindow.HideMetroDialogAsync(dialogLastDate);
        }

        async void dialogTimeoutSelection_CloseEvent()
        {
            await mainWindow.HideMetroDialogAsync(dialogTimeoutSelection);
        }

        async void BTNTimeout_Click(object sender, RoutedEventArgs e)
        {
            await mainWindow.ShowMetroDialogAsync(dialogTimeoutSelection);
        }

        void networkController_PublicIPFound()
        {
            LBLNetworkInformation.Dispatcher.Invoke(new ThreadStart(() => updateNetworkInfo()));
        }

        void updateNetworkInfo()
        {
            if (NetworkController.isHost)
            {
                LBLNetworkInformation.Text = "External IP : " + networkController.PublicIP + "\n";
                LBLNetworkInformation.Text += "Internal IP : " + networkController.PrivateIP + "\n";
                LBLNetworkInformation.Text += "Port           : " + EnvironmentInformation.portH + "\n";
            }
            else
            {
                LBLNetworkInformation.Text = "Connected IP : " + EnvironmentInformation.lastJoinIP + "\n";
                LBLNetworkInformation.Text += "Port                : " + EnvironmentInformation.lastJoinPort + "\n";
            }
        }

        void LBLNetworkInformation_Loaded(object sender, RoutedEventArgs e)
        {
            updateNetworkInfo();
        }

        void GameController_GameBoardCreated()
        {
            this.Dispatcher.BeginInvoke(new ThreadStart(() => gameBoardInitializedDisplay(GameController.GameBoard)));
        }

        void BTNNotReady_Click(object sender, RoutedEventArgs e)
        {
            GameController.sendNotReadyPacket();

            BTNReady.Visibility = System.Windows.Visibility.Visible;
            BTNNotReady.Visibility = System.Windows.Visibility.Hidden;
        }

        void BTNReady_Click(object sender, RoutedEventArgs e)
        {
            GameController.sendReadyPacket();

            BTNReady.Visibility = System.Windows.Visibility.Hidden;
            BTNNotReady.Visibility = System.Windows.Visibility.Visible;
        }

        void gameBoardInitializedDisplay(GameBoard gameBoard)
        {
            GameController.GameBoard.NewPlayerAdded += newPlayer;
            GameController.GameBoard.PlayerRemoved += gameBoard_PlayerRemoved;

            drawPlayers();            
        }

        void gameBoard_PlayerRemoved(object source, GameBoard.PlayerRemovedEvent e)
        {
            this.Dispatcher.Invoke(new ThreadStart(() => redrawPlayers()));
        }

        private void redrawPlayers()
        {
            clearCurrentDrawing();
            drawPlayers();
        }

        private void drawPlayers()
        {
            foreach (Player player in GameController.GameBoard.Players)
            {
                addPlayerToDisplay(player);
            }
        }

        private void clearCurrentDrawing()
        {
            while (playerDisplays.Count != 0)
            {
                WaitingRoomPlayer playerDisplay = playerDisplays[0];
                GBPlayersCanvas.Children.Remove(playerDisplay);
                playerDisplays.RemoveAt(0);
            }
        }

        void newPlayer(Player player)
        {
            this.Dispatcher.BeginInvoke(new ThreadStart(() => addPlayerToDisplay(player)));
        }

        void addPlayerToDisplay(Player player)
        {
            WaitingRoomPlayer playerDisplay = new WaitingRoomPlayer(player, NetworkController.isHost);
            playerDisplay.Height = PlayerIconHeight;
            playerDisplay.Width = PlayerIconWidth;
            playerDisplay.Margin = displayLocations[playerDisplays.Count];
            GBPlayersCanvas.Children.Add(playerDisplay);
            playerDisplays.Add(playerDisplay);
        }

        void TXTTextToSend_Loaded(object sender, RoutedEventArgs e)
        {
            TXTTextToSend.Focus();

            if (NetworkController.isHost)
            {
                BTNStart.Visibility = System.Windows.Visibility.Visible;
                BTNReady.Visibility = System.Windows.Visibility.Hidden;
            }
            else
            {
                BTNStart.Visibility = System.Windows.Visibility.Hidden;
                BTNReady.Visibility = System.Windows.Visibility.Visible;
            }
        }

        void TXTTextToSend_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                SendText();
            }
        }

        void BTNSend_Click(object sender, RoutedEventArgs e)
        {
            SendText();
        }

        void SendText()
        {
            string message = TXTTextToSend.Text.Trim();

            if (message != "")
            {
                networkController.sendMessage(TXTTextToSend.Text);
                TXTTextToSend.Text = "";
            }            
        }

        void networkController_ChatPacketReceived(JSONNetworkControllerPackets.ChatPacket chatPacket)
        {            
            TXTDiscussion.Dispatcher.Invoke(new ThreadStart(()=> AddTextToDiscussion(chatPacket)));
        }

        void AddTextToDiscussion(JSONNetworkControllerPackets.ChatPacket chatPacket)
        {
            TXTDiscussion.Text += chatPacket.Username + ": " + chatPacket.Message + "\n";
            TXTDiscussion.ScrollToEnd();
        }

        void BTNClose_Click(object sender, RoutedEventArgs e)
        {
            BTNCloseClick();
        }

        public void ResetChat()
        {
            TXTDiscussion.Text = "";
            TXTTextToSend.Text = "";
        }

        public void reset()
        {
            this.Dispatcher.Invoke(new ThreadStart(() => resetInvoke()));
        }

        async public void resetInvoke()
        {
            clearCurrentDrawing();
            ResetChat();
            BTNReady.Visibility = System.Windows.Visibility.Visible;
            BTNNotReady.Visibility = System.Windows.Visibility.Hidden;

            if (dialogTimeoutSelection.IsVisible)
            {
                await mainWindow.HideMetroDialogAsync(dialogTimeoutSelection);
            }

            if (dialogLastDate.IsVisible)
            {
                await mainWindow.ShowMetroDialogAsync(dialogLastDate);
            }
            
        }
    }
}
