﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LoveLetterWPF.Controllers;
using System.Threading;
using LoveLetterWPF.Menus.CustomControls;

namespace LoveLetterWPF.Menus
{
    /// <summary>
    /// Interaction logic for GameRoom.xaml
    /// </summary>
    public partial class GameRoom : UserControl
    {
        public double windowHeight = 800;
        public double windowWidth = 1000;

        NetworkController networkController;

        public GameRoom(NetworkController _networkController)
        {
            InitializeComponent();

            networkController = _networkController;

            BTNSend.Click += BTNSend_Click;
            TXTTextToSend.PreviewKeyDown += TXTTextToSend_PreviewKeyDown;
            TXTTextToSend.Loaded += TXTTextToSend_Loaded;

            networkController.ChatPacketReceived += networkController_ChatPacketReceived;
        }

        void TXTTextToSend_Loaded(object sender, RoutedEventArgs e)
        {
            TXTTextToSend.Focus();
        }

        void networkController_ChatPacketReceived(JSONNetworkControllerPackets.ChatPacket chatPacket)
        {
            TXTDiscussion.Dispatcher.Invoke(new ThreadStart(() => AddTextToDiscussion(chatPacket)));
        }

        void AddTextToDiscussion(JSONNetworkControllerPackets.ChatPacket chatPacket)
        {
            TXTDiscussion.Text += chatPacket.Username + ": " + chatPacket.Message + "\n";
            TXTDiscussion.ScrollToEnd();
        }

        void BTNSend_Click(object sender, RoutedEventArgs e)
        {
            SendText();
        }

        void TXTTextToSend_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                SendText();
            }
        }

        void SendText()
        {
            string message = TXTTextToSend.Text.Trim();

            if (message != "")
            {
                networkController.sendMessage(TXTTextToSend.Text);
                TXTTextToSend.Text = "";
            }
        }

        public void displayGame() 
        {
            
        }
    }
}
