﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LoveLetterWPF.Menus
{
    /// <summary>
    /// Interaction logic for MainMenuButtons.xaml
    /// </summary>
    public partial class MainMenuButtons : UserControl
    {
        SolidColorBrush goldTransparent;

        public delegate void buttonClick();

        public buttonClick BTNCreditsClick;
        public buttonClick BTNHowToClick;
        public buttonClick BTNJoinClick;
        public buttonClick BTNHostClick;
        public buttonClick BTNOptionsClick;

        public MainMenuButtons()
        {
            InitializeComponent();

            Color gold = (Color)this.Resources["AccentColor"];
            goldTransparent = new SolidColorBrush(gold);
            goldTransparent.Opacity = 0.5;

            BTNJoin.Background = goldTransparent;
            BTNHost.Background = goldTransparent;
            BTNHowTo.Background = goldTransparent;
            BTNOptions.Background = goldTransparent;
            BTNCredits.Background = goldTransparent;

            
            BTNHost.Click += BTNHost_Click;
            BTNJoin.Click += BTNJoin_Click;
            BTNOptions.Click += BTNOptions_Click;
            BTNHowTo.Click += BTNHowTo_Click;
            BTNCredits.Click += BTNCredits_Click;

        }

        void BTNHowTo_Click(object sender, RoutedEventArgs e)
        {
            BTNHowToClick();
        }

        void BTNOptions_Click(object sender, RoutedEventArgs e)
        {
            BTNOptionsClick();
        }

        void BTNJoin_Click(object sender, RoutedEventArgs e)
        {
            BTNJoinClick();
        }

        void BTNHost_Click(object sender, RoutedEventArgs e)
        {
            BTNHostClick();
        }

        void BTNCredits_Click(object sender, RoutedEventArgs e)
        {
            BTNCreditsClick();
        }
    }
}
