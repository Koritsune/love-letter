﻿using LoveLetterController.Controllers;
using LoveLetterWPF.Models;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LoveLetterWPF.Dialogs
{
    /// <summary>
    /// Interaction logic for DialogLastDate.xaml
    /// </summary>
    public partial class DialogLastDate : CustomDialog
    {
        public event CloseEventHandler CloseEvent;
        public delegate void CloseEventHandler();

        public DialogLastDate()
        {
            InitializeComponent();

            this.Resources.MergedDictionaries.Add(new System.Windows.ResourceDictionary() { Source = new Uri("pack://application:,,,/MahApps.Metro;component/Themes/Dialogs/BaseMetroDialog.xaml") });

            Style = (Style)Resources["WhiteDialogStyle"];

            BTNCancel.Click += BTNCancel_Click;
            BTNSave.Click += BTNSave_Click;

            DPLastDate.DisplayDateEnd = DateTime.Now;

            this.Loaded += DialogLastDate_Loaded;
        }

        void DialogLastDate_Loaded(object sender, RoutedEventArgs e)
        {
            DPLastDate.SelectedDate = EnvironmentInformation.LastDate;
        }

        void BTNCancel_Click(object sender, RoutedEventArgs e)
        {
            TriggerClose();
        }

        void BTNSave_Click(object sender, RoutedEventArgs e)
        {
            EnvironmentInformation.LastDate = (DateTime)DPLastDate.SelectedDate;
            GameController.sendNewDatePacket(EnvironmentInformation.LastDate);
            TriggerClose();
        }

        private void TriggerClose()
        {
            if (CloseEvent != null)
            {
                CloseEvent();
            }
        }
    }
}
