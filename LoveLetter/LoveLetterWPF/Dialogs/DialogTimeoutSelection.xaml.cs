﻿using LoveLetterController.Controllers;
using LoveLetterController.Models;
using LoveLetterWPF.Controllers;
using LoveLetterWPF.Models;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LoveLetterWPF.Dialogs
{
    /// <summary>
    /// Interaction logic for DialogTimeoutSelection.xaml
    /// </summary>
    public partial class DialogTimeoutSelection : CustomDialog
    {

        public event CloseHandler CloseEvent;
        public delegate void CloseHandler();
        
        public DialogTimeoutSelection()
        {
            InitializeComponent();

            this.Resources.MergedDictionaries.Add(new System.Windows.ResourceDictionary() { Source = new Uri("pack://application:,,,/MahApps.Metro;component/Themes/Dialogs/BaseMetroDialog.xaml") });

            Style = (Style)Resources["WhiteDialogStyle"];

            BTNSave.Click += BTNSave_Click;
            BTNCancel.Click += BTNCancel_Click;

            foreach (int timeout in Constants.TIMEOUT_LIST_SEC)
            {
                CBTimeout.Items.Add(timeout);
            }

            CBTimeout.SelectedIndex = EnvironmentInformation.Timeout_Index;

            this.Loaded += DialogTimeoutSelection_Loaded;
            GameBoard.TimeoutChanged += GameBoard_TimeoutChanged;
        }

        void GameBoard_TimeoutChanged()
        {
            CBTimeout.Dispatcher.Invoke(new ThreadStart(() => updateTimeout()));
        }

        void updateTimeout()
        {
            CBTimeout.SelectedItem = GameBoard.Timeout / 1000;
        }

        void DialogTimeoutSelection_Loaded(object sender, RoutedEventArgs e)
        {
            if (!NetworkController.isHost)
            {
                BTNCancel.Content = "Ok";
                BTNSave.Visibility = System.Windows.Visibility.Hidden;

                CBTimeout.SelectedItem = GameBoard.Timeout / 1000;
            }
            else
            {
                BTNCancel.Content = "Cancel";
                BTNSave.Visibility = System.Windows.Visibility.Visible;

                CBTimeout.SelectedIndex = EnvironmentInformation.Timeout_Index;
            }
        }

        void BTNCancel_Click(object sender, RoutedEventArgs e)
        {
            TriggerClose();
        }

        void BTNSave_Click(object sender, RoutedEventArgs e)
        {
            EnvironmentInformation.Timeout_Index = CBTimeout.SelectedIndex;
            GameController.sendTimeoutPacket(EnvironmentInformation.Timeout);
            TriggerClose();
        }

        private void TriggerClose()
        {
            if (CloseEvent != null)
            {
                CloseEvent();
            }
        }
    }
}
