﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls.Dialogs;
using LoveLetterWPF.Controllers;
using System.Text.RegularExpressions;
using LoveLetterWPF.Models;

namespace LoveLetterWPF.Dialogs
{
    /// <summary>
    /// Interaction logic for DialogOptions.xaml
    /// </summary>
    public partial class DialogOptions : CustomDialog
    {
        MainWindow mainWindow; //needed to show dialog

        public delegate void buttonClick();
        public buttonClick closing;

        public DialogOptions(MainWindow _mainWindow)
        {
            InitializeComponent();

            mainWindow = _mainWindow;

            ImgDisplay.Stretch = Stretch.Fill;
            ImgDisplay.Source = EnvironmentInformation.PlayerImage;

            BTNCancel.Click += BTNCancel_Click;
            BTNSave.Click += BTNSave_Click;

            this.Resources.MergedDictionaries.Add(new System.Windows.ResourceDictionary() { Source = new Uri("pack://application:,,,/MahApps.Metro;component/Themes/Dialogs/BaseMetroDialog.xaml") });

            Uri uriPrev = new Uri("../Pictures/Button Icons/rw.png", UriKind.Relative);
            IMGBTNPrevImg.Source = new BitmapImage(uriPrev);

            Uri uriNext = new Uri("../Pictures/Button Icons/ff.png",UriKind.Relative);
            IMGBTNNextImg.Source = new BitmapImage(uriNext);

            BTNNextImg.Click += BTNNextImg_Click;
            BTNPrevImg.Click += BTNPrevImg_Click;

            Loaded += DialogOptions_Loaded;

            TXTPortH.PreviewTextInput += TXTPortH_PreviewTextInput;
        }
        void TXTPortH_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !isPositiveInteger(TXTPortH.Text + e.Text);
        }

        private static bool isPositiveInteger(string text)
        {
            Regex regex = new Regex("^[0-9]*$"); //regex that matches disallowed text
            return regex.IsMatch(text);
        }

        void DialogOptions_Loaded(object sender, RoutedEventArgs e)
        {
            TXTUsername.Text = EnvironmentInformation.Username;
            TXTPortH.Text = "" + EnvironmentInformation.portH;

            ImgDisplay.Source = EnvironmentInformation.PlayerImage;
                        
            BTNNextImg.IsEnabled = EnvironmentInformation.hasNextImage();
            BTNPrevImg.IsEnabled = EnvironmentInformation.hasPrevImage();            
        }

        void BTNPrevImg_Click(object sender, RoutedEventArgs e)
        {
            EnvironmentInformation.prevImg();
            ImgDisplay.Source = EnvironmentInformation.PlayerImage;

            if (EnvironmentInformation.hasNextImage())
            {
                BTNNextImg.IsEnabled = true;
            }

            if (!EnvironmentInformation.hasPrevImage())
            {
                BTNPrevImg.IsEnabled = false;
            }
        }

        void BTNNextImg_Click(object sender, RoutedEventArgs e)
        {
            EnvironmentInformation.nextImg();
            ImgDisplay.Source = EnvironmentInformation.PlayerImage;

            if (!EnvironmentInformation.hasNextImage())
            {
                BTNNextImg.IsEnabled = false;
            }

            if (EnvironmentInformation.hasPrevImage())
            {
                BTNPrevImg.IsEnabled = true;
            }
        }

        void BTNSave_Click(object sender, RoutedEventArgs e)
        {
            if (TXTUsername.Text.Length < Constants.USERNAME_LENGTH_MINIMUM || TXTUsername.Text.Length > Constants.USERNAME_LENGTH_MAXIMUM)
            {
                mainWindow.ShowMessageAsync("Invalid Username", Constants.MESSAGE_USERNAME_LENGTH_INVALID);
            }
            else
            {
                EnvironmentInformation.Username = TXTUsername.Text;

                int port;
                Int32.TryParse(TXTPortH.Text, out port);

                EnvironmentInformation.portH = port;

                EnvironmentInformation.saveToFile();
                closingOptions();
            }
        }

        void BTNCancel_Click(object sender, RoutedEventArgs e)
        {
            EnvironmentInformation.initConfiguration();
            closingOptions();
        }
        public void closingOptions()
        {
            closing();
        }
    }
}
