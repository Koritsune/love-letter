﻿using LoveLetterWPF.Controllers;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LoveLetterWPF.Dialogs
{
    /// <summary>
    /// Interaction logic for DialogJoinGame.xaml
    /// </summary>
    public partial class DialogJoinGame : CustomDialog
    {
        public DialogClosing Connected;
        public DialogClosing Cancel;        
        public delegate void DialogClosing();

        public string HostName;
        public int Port;

        NetworkController networkController;

        public DialogJoinGame(NetworkController _networkController)
        {
            InitializeComponent();

            networkController = _networkController;

            this.Resources.MergedDictionaries.Add(new System.Windows.ResourceDictionary() { Source = new Uri("pack://application:,,,/MahApps.Metro;component/Themes/Dialogs/BaseMetroDialog.xaml") });

            BTNJoin.Click += BTNJoin_Click;
            BTNCancel.Click += BTNCancel_Click;

            TXTPort.PreviewTextInput += TXTPort_PreviewTextInput;

            Loaded += DialogJoinGame_Loaded;
        }

        void DialogJoinGame_Loaded(object sender, RoutedEventArgs e)
        {
            TXTHost.Text = HostName;
            TXTPort.Text = "" + Port;
        }        

        void BTNCancel_Click(object sender, RoutedEventArgs e)
        {
            if (Cancel != null)
            {
                Cancel();
            }
        }

        private void BTNJoin_Click(object sender, RoutedEventArgs e)
        {
            HostName = TXTHost.Text;
            Port = Int32.Parse(TXTPort.Text);

            if (networkController.Connect(HostName, Port))
            {
                if (Connected != null)
                {
                    Connected();
                }
            }
            else
            {
                networkController.CloseConnections();
            }
        }

        void TXTPort_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !isPositiveInteger(TXTPort.Text + e.Text);
        }

        private static bool isPositiveInteger(string text)
        {
            Regex regex = new Regex("^[0-9]*$"); //regex that matches disallowed text
            return regex.IsMatch(text);
        }
    }
}
