﻿using Restaurant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoveLetterWPF.Models;
using System.Windows.Media.Imaging;
using LoveLetterController.Models;
using LoveLetterController.Controllers;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;

namespace LoveLetterWPF.Controllers
{
    public class NetworkController
    {
        Server server;
        Waiter waiter;

        //public string PublicIP = "Discovering...";
        public string PublicIP = "Uncomment NetworkController Constructor";
        public string PrivateIP;

        public event PublicIPFoundHandler PublicIPFound;
        public delegate void PublicIPFoundHandler();

        volatile bool waitingForApproval = false;
        public static bool isHost = false;
        volatile bool waitingGameJoinRequest = true;
        volatile bool joinApproved = false;

        Server.NewWaiterForApprovalEvent WaiterApprovalEvent;

        public ChatPacketReceivedHandler ChatPacketReceived;
        public delegate void ChatPacketReceivedHandler(JSONNetworkControllerPackets.ChatPacket chatPacket);

        public event ConnectionRejectionEventHandler ConnectionRejected;
        public delegate void ConnectionRejectionEventHandler(object source, ConnectionRejectedEventArgs e);
        public class ConnectionRejectedEventArgs : EventArgs
        {
            public string Reason { get; set;}

            public ConnectionRejectedEventArgs(string reason)
            {
                Reason = reason;
            }
        }

        public event ConnectionStatusChangedEventHandler ConnectionStatusChanged;
        public delegate void ConnectionStatusChangedEventHandler(object source, ConnectionStatusChangedEventArgs e);
        public class ConnectionStatusChangedEventArgs : EventArgs
        {
            public int PlayerID { get; set; }

            public ConnectionStatusChangedEventArgs(int ID)
            {
                PlayerID = ID;
            }
        }

        public event ConnectionClosedEventHandler ConnectionLost;
        public event ConnectionClosedEventHandler ConnectionClosed;
        public delegate void ConnectionClosedEventHandler(); 

        public NetworkController()
        {
            GetPrivateIP();
            //new Task(GetPublicIP).Start(); //run asynchronously because this process sometimes takes 10 seconds

            server = new Server();

            server.RequireApprovalForWaiters = true;
            server.ServerMessageReceived += server_ServerMessageReceived;
            server.NewWaiterForApproval += server_NewWaiterForApproval;
            server.NewWaiterAdded += server_NewWaiterAdded;
            server.WaiterStatusChanged += server_WaiterStatusChanged;

            waiter = new Waiter();
            waiter.MessageReceived += waiter_MessageReceived;
            waiter.ConnectionDeclined += waiter_ConnectionDeclined;
            waiter.LostConnection += waiter_LostConnection;
            waiter.QuitReceived += waiter_QuitReceived;

            GameController.GamePacketSendRequest += GameController_GamePacketSendRequest;
        }

        void server_WaiterStatusChanged(object source, Server.WaiterStatusChangeEvent e)
        {
            Player playerStatusChanged = GameController.GameBoard.getPlayer(GameController.GameBoard.getUsernameFromID(e.ServerID));


        }

        void GameController_GamePacketSendRequest(JSONGameControllerPackets.GamePacket gamePacket)
        {
            string networkPacket = JSONNetworkControllerPackets.generateNetworkPacketStringFromGamePacket(gamePacket);

            waiter.SendMessage(networkPacket);
        }

        void server_NewWaiterAdded(object source, Server.NewWaiterAddedEvent e)
        {
            e.WaiterAdded.SendMessage(JSONNetworkControllerPackets.generateGameBoardPacketString(GameController.GameBoard));
            e.WaiterAdded.StatusChanged += WaiterAdded_StatusChanged;

            e.WaiterAdded.LostConnection += WaiterAdded_LostConnection;
            e.WaiterAdded.QuitReceived += WaiterAdded_QuitReceived;
        }

        private void GetPrivateIP()
        {
            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                var addr = ni.GetIPProperties().GatewayAddresses.FirstOrDefault();
                if (addr != null && !addr.Address.ToString().Equals("0.0.0.0"))
                {
                    if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                    {
                        foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                        {
                            if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                            {
                                PrivateIP = ip.Address.ToString();
                                return;
                            }
                        }
                    }
                }
            }

            PrivateIP = "Failed to Discover";
        }

        private void GetPublicIP()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] a = response.Split(':');
            string a2 = a[1].Substring(1);
            string[] a3 = a2.Split('<');
            string a4 = a3[0];

            PublicIP = a4;

            if (PublicIPFound != null)
            {
                PublicIPFound();
            }
            
        }

        void WaiterAdded_QuitReceived(object source, Waiter.MessageReceivedEvent e)
        {
            Waiter waiter = (Waiter)source;

            string username = GameController.GameBoard.getUsernameFromID(waiter.ServerID);

            if (username != null) //if player was not already removed because they were kicked
            {
                sendRemovePlayerPacket(username);
            }            
        }

        void WaiterAdded_LostConnection(object source, Waiter.LostConnectionEvent e)
        {
            Waiter waiter = (Waiter)source;

            string username = GameController.GameBoard.getUsernameFromID(waiter.ServerID);

            if (username != null) //if player was not already removed because they were kicked
            {
                sendRemovePlayerPacket(username);
            }            
        }

        void WaiterAdded_StatusChanged(object source, Waiter.StatusChangeEvent e)
        {
            if (ConnectionStatusChanged != null)
            {
                ConnectionStatusChangedEventArgs e2 = new ConnectionStatusChangedEventArgs(e.ServerID);
                ConnectionStatusChanged(this, e2);
            }
        }

        void waiter_ConnectionDeclined(object source, Waiter.ConnectionDeclinedEvent e)
        {
            joinApproved = false;
            waitingGameJoinRequest = false;

            if (ConnectionRejected != null)
            {
                ConnectionRejectedEventArgs e2 = new ConnectionRejectedEventArgs(e.DeclinedMessage);

                ConnectionRejected(this, e2);
            }
        }

        void waiter_MessageReceived(object source, Waiter.MessageReceivedEvent e)
        {
            JSONNetworkControllerPackets.NetworkPacket networkPacket = JSONNetworkControllerPackets.generateNetworkPacketFromString(e.message);

            if (networkPacket.Message_Type == JSONNetworkControllerPackets.MSGType.CHAT && ChatPacketReceived != null)
            {
                JSONNetworkControllerPackets.ChatPacket chatPacket = JSONNetworkControllerPackets.generateChatPacketFromString(networkPacket.Content);

                ChatPacketReceived(chatPacket);
            }
            else if (networkPacket.Message_Type == JSONNetworkControllerPackets.MSGType.GAME)
            {
                JSONGameControllerPackets.GamePacket gamePacket = JSONGameControllerPackets.generateGamePacketFromString(networkPacket.Content);

                GameController.processGamePacket(gamePacket);
            }
            else if (networkPacket.Message_Type == JSONNetworkControllerPackets.MSGType.NEW_PLAYER)
            {
                Player newPlayer = JSONNetworkControllerPackets.generateNewPlayerFromString(networkPacket.Content);

                if (!isHost)
                {
                    GameController.GameBoard.addPlayer(newPlayer);
                }

            }
            else if (networkPacket.Message_Type == JSONNetworkControllerPackets.MSGType.REMOVE_PLAYER)
            {
                JSONNetworkControllerPackets.RemovePlayerPacket removePlayerPacket = JSONNetworkControllerPackets.generateRemovePlayerPacketFromString(networkPacket.Content);
                GameController.GameBoard.removePlayer(removePlayerPacket.Username);
            }
            else if (networkPacket.Message_Type == JSONNetworkControllerPackets.MSGType.GAME_BOARD)
            {
                if (!isHost)
                {
                    GameBoard gameBoard = JSONNetworkControllerPackets.generateGameBoardFromString(networkPacket.Content);
                    GameController.GameBoard = gameBoard;
                }                

                waitingGameJoinRequest = false;
                joinApproved = true;
            }

        }

        public void Host()
        {
            isHost = true;
            server.AcceptConnections("0.0.0.0", EnvironmentInformation.portH);
        }

        public void SelfConnect()
        {
            Connect("127.0.0.1", EnvironmentInformation.portH);
        }

        public bool Connect(string Host, int Port)
        {
            try
            {
                waitingGameJoinRequest = true;
                waiter.ConnectToServer(Host, Port);
                waiter.SendMessage(JSONNetworkControllerPackets.generateConnectionRequestPacketString(new Player(EnvironmentInformation.Username, EnvironmentInformation.DisplayPictureIndex, EnvironmentInformation.LastDate, isHost)));
            }
#pragma warning disable
            catch (System.Net.Sockets.SocketException e)
            {
#pragma warning enable

                if (ConnectionRejected != null)
                {
                    ConnectionRejectedEventArgs e2 = new ConnectionRejectedEventArgs(LoveLetterWPF.Models.Constants.MESSAGE_CONNECTION_FAILED);
                    ConnectionRejected(this, e2);
                }

                return false;
            }

            while (waitingGameJoinRequest) ;

            return joinApproved;
        }

        void waiter_QuitReceived(object source, Waiter.MessageReceivedEvent e)
        {
            if (!isHost && ConnectionClosed != null)
            {
                ConnectionClosed();
            }
        }

        void waiter_LostConnection(object source, Waiter.LostConnectionEvent e)
        {
            if (!isHost && ConnectionLost != null)
            {
                ConnectionLost();
            }
        }

        void server_NewWaiterForApproval(object source, Server.NewWaiterForApprovalEvent e)
        {
            waitingForApproval = true;

            WaiterApprovalEvent = e;

            while (waitingForApproval) ; //prevent other players from joining while we are evaluating if the current player can join or not           
        }

        void server_ServerMessageReceived(object source, Server.ServerMessageReceivedEvent e)
        {
            JSONNetworkControllerPackets.NetworkPacket gamePacket = JSONNetworkControllerPackets.generateNetworkPacketFromString(e.message);

            if (gamePacket.Message_Type == JSONNetworkControllerPackets.MSGType.CONNECTION_REQUEST)
            {
                JSONNetworkControllerPackets.ConnectionRequestPacket connectionRequestPacket = JSONNetworkControllerPackets.generateConnectionRequestPacketFromString(gamePacket.Content);

                if (connectionRequestPacket.Version != GameConstants.VERSION)
                {
                    WaiterApprovalEvent.Approved = false;
                    WaiterApprovalEvent.DeclinedMessage = LoveLetterWPF.Models.Constants.MESSAGE_VERSION_MISMATCH + connectionRequestPacket.Version;
                }
                else if (GameBoard.GameStarted) //TODO: Enable a reconnection for a player who lost connection or quit
                {
                    WaiterApprovalEvent.Approved = false;
                    WaiterApprovalEvent.DeclinedMessage = LoveLetterWPF.Models.Constants.MESSAGE_GAME_STARTED;
                }
                else if (GameController.GameBoard.doesUsernameExist(connectionRequestPacket.NewPlayer.Username))
                {
                    WaiterApprovalEvent.Approved = false;
                    WaiterApprovalEvent.DeclinedMessage = LoveLetterWPF.Models.Constants.MESSAGE_DUPLICATE_USERNAME + connectionRequestPacket.NewPlayer.Username;
                }
                else if (!GameController.GameBoard.canAddAnotherPlayer())
                {
                    WaiterApprovalEvent.Approved = false;
                    WaiterApprovalEvent.DeclinedMessage = LoveLetterWPF.Models.Constants.MESSAGE_MAX_PLAYERS;
                }
                else
                {
                    connectionRequestPacket.NewPlayer.ID = e.ServerID;
                    GameController.GameBoard.addPlayer(connectionRequestPacket.NewPlayer);
                    connectionRequestPacket.NewPlayer.Removed += NewPlayer_Removed;
                    //send new player to others this occurs before game board is sent to new player since he has not been added to the server list yet
                    server.SendMessage(JSONNetworkControllerPackets.generateNewPlayerPacketString(connectionRequestPacket.NewPlayer));
                    WaiterApprovalEvent.Approved = true;
                }

                waitingForApproval = false;
                
            }
            else 
            {
                server.SendMessage(e.message);
            }
        }

        void NewPlayer_Removed(Player player)
        {
            sendRemovePlayerPacket(player.Username);
        }

        public void sendMessage(string message)
        {
            string chatPacketString = JSONNetworkControllerPackets.generateChatPacketString(EnvironmentInformation.Username, message);
            waiter.SendMessage(chatPacketString);
        }

        public void sendGamePacket(JSONGameControllerPackets.GamePacket packet)
        {
            string gamePacketString = JSONNetworkControllerPackets.generateNetworkPacketStringFromGamePacket(packet);
            waiter.SendMessage(gamePacketString);
        }

        public void sendRemovePlayerPacket(string username)
        {
            string kickPacketString = JSONNetworkControllerPackets.generateRemovePlayerPacketString(username);
            waiter.SendMessage(kickPacketString);
        }

        public void CloseConnections()
        {
            server.CloseConnections();
            waiter.GracefulClose();

            joinApproved = false;
            waitingGameJoinRequest = false;
            waitingForApproval = false;
            isHost = false;
        }
    }
}
