﻿using LoveLetterController.Controllers;
using LoveLetterController.Models;
using LoveLetterWPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace LoveLetterWPF.Controllers
{
    public static class JSONNetworkControllerPackets
    {
        static JavaScriptSerializer JSONSerializer = new JavaScriptSerializer();

        public enum MSGType
        {
            CONNECTION_REQUEST,
            GAME_BOARD,
            NEW_PLAYER,
            CHAT,
            GAME,
            REMOVE_PLAYER
        }        

        public class NetworkPacket
        {
            public MSGType Message_Type { get; set; }
            public string Content { get; set; }

            public NetworkPacket()
            {

            }

            public NetworkPacket(MSGType message_Type, string content)
            {
                Message_Type = message_Type;
                Content = content;
            }
        }

        public class RemovePlayerPacket
        {
            public string Username { get; set; }

            public RemovePlayerPacket()
            {
            
            }

            public RemovePlayerPacket(string username)
            {
                Username = username;
            }
        }

        public class ChatPacket
        {
            public string Username { get; set; }
            public string Message { get; set; }

            public ChatPacket()
            {

            }

            public ChatPacket(string username, string message)
            {
                Username = username;
                Message = message;
            }
        }

        public class ConnectionRequestPacket
        {
            public string Version { get; set; }
            public Player NewPlayer;

            public ConnectionRequestPacket()
            {

            }

            public ConnectionRequestPacket(string version, Player newPlayer)
            {
                Version = version;
                NewPlayer = newPlayer;
            }
        }

        public static NetworkPacket generateNetworkPacketFromString(string content)
        {
            NetworkPacket packet = JSONSerializer.Deserialize<NetworkPacket>(content);

            return packet;
        }

        public static ChatPacket generateChatPacketFromString(string content)
        {
            ChatPacket packet = JSONSerializer.Deserialize<ChatPacket>(content);

            return packet;
        }        

        public static string generateChatPacketString(string username, string message)
        {
            ChatPacket chatPacket = new ChatPacket(username, message);
            string chatPacketString = JSONSerializer.Serialize(chatPacket);

            NetworkPacket gamePacket = new NetworkPacket(MSGType.CHAT, chatPacketString);

            return JSONSerializer.Serialize(gamePacket);
        }

        public static ConnectionRequestPacket generateConnectionRequestPacketFromString(string content)
        {
            ConnectionRequestPacket packet = JSONSerializer.Deserialize<ConnectionRequestPacket>(content);

            return packet;
        }

        public static string generateConnectionRequestPacketString(Player newPlayer)
        {
            ConnectionRequestPacket packet = new ConnectionRequestPacket(GameConstants.VERSION, newPlayer);
            string ConnectionRequestPacketString = JSONSerializer.Serialize(packet);

            NetworkPacket gamePacket = new NetworkPacket(MSGType.CONNECTION_REQUEST, ConnectionRequestPacketString);

            return JSONSerializer.Serialize(gamePacket);
        }

        public static GameBoard generateGameBoardFromString(string content)
        {
            GameBoard gameBoard = JSONSerializer.Deserialize<GameBoard>(content);

            return gameBoard;
        }

        public static string generateGameBoardPacketString(GameBoard gameBoard)
        {
            string gameBoardString = JSONSerializer.Serialize(gameBoard);

            NetworkPacket gamePacket = new NetworkPacket(MSGType.GAME_BOARD, gameBoardString);

            return JSONSerializer.Serialize(gamePacket);
        }

        public static Player generateNewPlayerFromString(string content)
        {
            Player newPlayer = JSONSerializer.Deserialize<Player>(content);

            return newPlayer;
        }

        public static string generateNewPlayerPacketString(Player newPlayer)
        {
            string newPlayerString = JSONSerializer.Serialize(newPlayer);

            NetworkPacket gamePacket = new NetworkPacket(MSGType.NEW_PLAYER, newPlayerString);

            return JSONSerializer.Serialize(gamePacket);
        }

        public static string generateNetworkPacketStringFromGamePacket(JSONGameControllerPackets.GamePacket gamePacket)
        {
            string content = JSONSerializer.Serialize(gamePacket);

            NetworkPacket networkPacket = new NetworkPacket(MSGType.GAME, content);

            return JSONSerializer.Serialize(networkPacket);
        }

        public static RemovePlayerPacket generateRemovePlayerPacketFromString(string content) 
        {
            RemovePlayerPacket kickPacket = JSONSerializer.Deserialize<RemovePlayerPacket>(content);

            return kickPacket;
        }

        public static string generateRemovePlayerPacketString(string username)
        {
            RemovePlayerPacket removePlayerPacket = new RemovePlayerPacket(username);
            string content = JSONSerializer.Serialize(removePlayerPacket);

            NetworkPacket networkPacket = new NetworkPacket(MSGType.REMOVE_PLAYER, content);

            return JSONSerializer.Serialize(networkPacket);
        }
    }
}
