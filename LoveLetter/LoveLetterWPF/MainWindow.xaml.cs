﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LoveLetterWPF.Menus;
using LoveLetterWPF.Dialogs;
using LoveLetterWPF.Controllers;
using LoveLetterWPF.Models;
using System.Threading;
using LoveLetterController.Models;
using LoveLetterController.Controllers;
using System.Windows.Forms;
using LoveLetterWPF.Menus.CustomControls;

namespace LoveLetterWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public static NetworkController _networkController = new NetworkController();
        public static GameController _gameController = new GameController();
        MainMenuButtons _mainMenuButtons = new MainMenuButtons();
        public static GameRoom _gameRoom = new GameRoom(_networkController);

        DialogOptions _dialogOptions;
        static WaitingRoom _waitingRoom;
        DialogJoinGame _dialogJoinGame;

        StateManager _stateManager;        

        private class StateManager
        {
            private enum MenuState
            {
                MAIN_MENU,
                WAITING_ROOM,
                IN_GAME
            }

            private struct StateContent
            {
                public Object content;
                public double width;
                public double height;

                public StateContent(Object Content, double Width, double Height)
                {
                    content = Content;
                    width = Width;
                    height = Height;
                }
            }

            MainWindow mainWindow;

            MenuState current_state;
            StateContent mainMenuContent;
            StateContent waitingRoomContent;
            StateContent gameRoomContent;


            public StateManager(MainWindow _mainWindow, WaitingRoom _waitingRoom)
            {
                mainWindow = _mainWindow;
                current_state = MenuState.MAIN_MENU;
                
                mainMenuContent = new StateContent(_mainWindow.Content,_mainWindow.Width,_mainWindow.Height);
                waitingRoomContent = new StateContent(_waitingRoom.Content, _waitingRoom.windowWidth, _waitingRoom.windowHeight);
                gameRoomContent = new StateContent(_gameRoom.Content, _gameRoom.windowWidth, _gameRoom.windowHeight);
            }

            private void centerOnScreen()
            {
                Screen screen = Screen.FromHandle(new System.Windows.Interop.WindowInteropHelper(mainWindow).Handle);
                mainWindow.Left = screen.Bounds.X + ((screen.Bounds.Width - mainWindow.ActualWidth) / 2);
                mainWindow.Top = screen.Bounds.Y + ((screen.Bounds.Height - mainWindow.ActualHeight) / 2); 
            }

            public void toWaitingRoom()
            {
                clearState();

                current_state = MenuState.WAITING_ROOM;                            

                mainWindow.Height = waitingRoomContent.height;
                mainWindow.Width = waitingRoomContent.width;
                mainWindow.Content = waitingRoomContent.content;

                GameBoard.Username = EnvironmentInformation.Username;

                _waitingRoom.ResetChat();

                centerOnScreen();
            }

            private void clearState()
            {
                if (current_state == MenuState.WAITING_ROOM)
                {
                    GameController.GameBoard.clearBoard();
                    _waitingRoom.reset();
                    _networkController.CloseConnections();
                }
                else if (current_state == MenuState.IN_GAME)
                {
                    GameController.GameBoard.clearBoard();
                    _waitingRoom.reset();
                    _networkController.CloseConnections();
                }
                else if (current_state == MenuState.MAIN_MENU)
                {

                }
            }

            public void toMainMenu()
            {
                clearState();

                if (current_state == MenuState.IN_GAME || current_state == MenuState.WAITING_ROOM)
                {                    
                    _networkController.CloseConnections();
                }

                current_state = MenuState.MAIN_MENU;

                mainWindow.Height = mainMenuContent.height;
                mainWindow.Width = mainMenuContent.width;
                mainWindow.Content = mainMenuContent.content;

                centerOnScreen();
            }

            public void toGameWindow() 
            {
                //clearState(); not needed because nothing needs to be cleared from waitingroom to gameRoom

                current_state = MenuState.IN_GAME;

                mainWindow.Height = gameRoomContent.height;
                mainWindow.Width = gameRoomContent.width;
                mainWindow.Content = gameRoomContent.content;

                _gameRoom.displayGame();

                centerOnScreen();
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            Image imgLoader = new Image();
            imgLoader.Visibility = System.Windows.Visibility.Hidden;
            canvas.Children.Add(imgLoader);
            EnvironmentInformation.InitializeEnvironnement(imgLoader);
            canvas.Children.Remove(imgLoader);
            
            _dialogOptions = new DialogOptions(this);
            //_dialogOptions.Style = (Style)Resources["WhiteDialogStyle"];
            _dialogOptions.Style = (Style)Resources["RedTransparentDialogStyle"];
            _dialogOptions.closing += closeOptions;

            canvas.Children.Add(_mainMenuButtons);            

            _dialogJoinGame = new DialogJoinGame(_networkController);
            _dialogJoinGame.Style = (Style)Resources["WhiteDialogStyle"];
            _dialogJoinGame.Connected += dialogJoinGame_Connected;
            _dialogJoinGame.Cancel += dialogJoinGame_Closing;
            _dialogJoinGame.HostName = EnvironmentInformation.lastJoinIP;
            _dialogJoinGame.Port = EnvironmentInformation.lastJoinPort;

            _mainMenuButtons.BTNHostClick += HostClick;
            _mainMenuButtons.BTNJoinClick += JoinClick;
            _mainMenuButtons.BTNCreditsClick += CreditsClick;
            _mainMenuButtons.BTNHowToClick += HowToClick;
            _mainMenuButtons.BTNOptionsClick += OptionsClick;            

            Closing += MainWindow_Closing;

            Loaded += MainWindow_Loaded;            

            _networkController.ConnectionRejected += _networkController_ConnectionRejected;
            _networkController.ConnectionLost += _networkController_ConnectionLost;
            _networkController.ConnectionClosed += _networkController_ConnectionClosed;

            GameController.init();
            GameController.GameBoard.PlayerRemoved += gameBoard_PlayerRemoved;
            GameController.GameBoardCreated += GameController_GameBoardCreated;

            _waitingRoom = new WaitingRoom(this);
            _waitingRoom.BTNCloseClick += waitingRoomCloseClick;

            GameBoard.GameStartedEvent += gameStartTrigger;

            EnemyPlayer enemy = new EnemyPlayer(new Player("Luap",0,DateTime.Now,true));
            canvas.Children.Add(enemy);
        }

        void gameStartTrigger() 
        {
            this.Dispatcher.Invoke(new ThreadStart(() => _stateManager.toGameWindow()));
        }

        void GameController_GameBoardCreated() 
        {
            //event listener needs to be re-set if gameboard is received from a server
            GameController.GameBoard.PlayerRemoved += gameBoard_PlayerRemoved;
        }

        void _networkController_ConnectionClosed()
        {
            this.Dispatcher.Invoke(new ThreadStart(() => lostConnectionInvoke(Constants.MESSAGE_SERVER_CLOSED)));
        }

        void _networkController_ConnectionLost()
        {
            this.Dispatcher.Invoke(new ThreadStart(() => lostConnectionInvoke(Constants.MESSAGE_SERVER_LOST)));
        }

        async void lostConnectionInvoke(string reason)
        {
            _stateManager.toMainMenu();
            await this.ShowMessageAsync("Lost Connection", reason);
        }

        void gameBoard_PlayerRemoved(object source, GameBoard.PlayerRemovedEvent e)
        {
            if (e.RemovedPlayer.Username == EnvironmentInformation.Username)
            {
                this.Dispatcher.Invoke(new ThreadStart(() => lostConnectionInvoke(e.Reason)));                
            }
        }

        async void dialogJoinGame_Connected()
        {
            EnvironmentInformation.lastJoinIP = _dialogJoinGame.HostName;
            EnvironmentInformation.lastJoinPort = _dialogJoinGame.Port;

            _stateManager.toWaitingRoom();
            await this.HideMetroDialogAsync(_dialogJoinGame);
        }

        async void dialogJoinGame_Closing()
        {
            await this.HideMetroDialogAsync(_dialogJoinGame);
        }

        void _networkController_ConnectionRejected(object source, NetworkController.ConnectionRejectedEventArgs e)
        {
            this.Dispatcher.Invoke(new ThreadStart(() => gameJoinFailed(e.Reason)));
        }

        async private void gameJoinFailed(string reason)
        {
            _stateManager.toMainMenu();
            await this.ShowMessageAsync("Connection Error", reason);
        }

        private void waitingRoomCloseClick()
        {
            _stateManager.toMainMenu();
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _stateManager = new StateManager(this, _waitingRoom);
        }

        async private void JoinClick()
        {
            await this.ShowMetroDialogAsync(_dialogJoinGame);
        }

        private void HostClick()
        {
            _networkController.Host();
            _networkController.SelfConnect();
            _stateManager.toWaitingRoom();
        }

        void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            EnvironmentInformation.saveToFile();
            _networkController.CloseConnections();
        }

        private void closeOptions()
        {
            this.HideMetroDialogAsync(_dialogOptions);
        }

        private void OptionsClick()
        {
            this.ShowMetroDialogAsync(_dialogOptions);
        }

        public void HowToClick()
        {
            System.Diagnostics.Process.Start("https://www.youtube.com/watch?v=6XHpHrKtIEM");
        }

        async public void CreditsClick()
        {
            await this.ShowMessageAsync("Credits", "Programmers: Koritsune" + "\n" + "Game Designer: Seiji Kanai" + "\n" + "Youtube Instructions: King Me");
        }
        
    }
}