﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoveLetterController.Models;

namespace LoveLetterWPF.Models
{
    public static class Constants
    {   
        
        public const int DEFAULT_TIMEOUT_INDEX = 0;
        public static readonly int[] TIMEOUT_LIST_SEC = { 30, 20, 15, 10 };
        public static readonly int[] TIMEOUT_LIST_MS = { TIMEOUT_LIST_SEC[0] * 1000, TIMEOUT_LIST_SEC[1] * 1000, TIMEOUT_LIST_SEC[2] * 1000, TIMEOUT_LIST_SEC[3] * 1000 };

        public const string MESSAGE_VERSION_MISMATCH = "The server has version: " + GameConstants.VERSION + " and you have version: ";
        public const string MESSAGE_DUPLICATE_USERNAME = "There is already a user connected by the name: ";
        public const string MESSAGE_CONNECTION_FAILED = "No connection could be established at the given host and port. Please ensure you have entered the right information or try again later.";
        public const string MESSAGE_GAME_STARTED = "The game has already started, maybe if you're really nice you can join the next one!";
        public static readonly string MESSAGE_MAX_PLAYERS = "The maximum number of players: " + GameConstants.MAX_PLAYERS + " has already been reached.";

        public const int USERNAME_LENGTH_MINIMUM = 4;
        public const int USERNAME_LENGTH_MAXIMUM = 16;
        public static readonly string MESSAGE_USERNAME_LENGTH_INVALID = "A username must be between " + USERNAME_LENGTH_MINIMUM + " and " + USERNAME_LENGTH_MAXIMUM + " characters.";

        public const string MESSAGE_SERVER_CLOSED = "The server closed the connection.";
        public const string MESSAGE_SERVER_LOST = "The connection to the server was lost.";
    }
}
