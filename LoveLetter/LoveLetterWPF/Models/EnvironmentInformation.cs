﻿using LoveLetterController.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Xml;

namespace LoveLetterWPF.Models
{
    public class EnvironmentInformation
    {
        public static string  lastJoinIP;
        public static int     lastJoinPort;

        public const string DATE_FORMAT = "MM-dd-yyyy";
        public static DateTime LastDate;

        public static int Timeout_Index;

        public static int Timeout
        {
            get
            {
                return Constants.TIMEOUT_LIST_MS[Timeout_Index];
            }
        }

        const int DEFAULT_HOST_PORT = 6114;
        const int DEFAULT_JOIN_PORT = 6114;
        const string DEFAULT_LAST_JOIN_IP = "127.0.0.1";
        
        public static int portH;

        public static string Username;
        public static int DisplayPictureIndex;

        const int DEFAULT_DISPLAY_PICTURE = 0;

        static string CONFIG_FILE = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Love Letter\\Config.XML";

        const string iconImageBaseFolder = "../Pictures/Player Icons/";
        const string playingCardBaseFolder = "../Pictures/Cards/";
        static string[] roles = { "Guard", "Priest", "Baron", "Handmaid", "Prince", "King", "Countess", "Princess" };
        const string IMAGE_EXTENSION = ".PNG";

        public static Image ImgLoader;

        const int NUM_DISPLAY_PICTURES = 8;
        static BitmapImage[] _iconPictures = new BitmapImage[NUM_DISPLAY_PICTURES];

        static public BitmapImage[] IconPictures
        {
            get
            {
                return _iconPictures;
            }
        }

        const int NUM_PLAYING_CARD_PICTURES = 9;
        static BitmapImage[] _playingCardPictures = new BitmapImage[NUM_PLAYING_CARD_PICTURES];

        public static BitmapImage[] PlayingCardPictures
        {
            get
            {
                return _playingCardPictures;
            }
        }

        public static BitmapImage PlayerImage
        {
            get
            {
                return _iconPictures[DisplayPictureIndex];
            }
        }

        public static void InitializeEnvironnement(Image imgLoader)
        {
            ImgLoader = imgLoader;
            initDisplayPictures();
            initPlayingCardPictures();
            initConfiguration();
        }

        private static void initDisplayPictures()
        {
            Uri uri;

            for (int count = 0; count < NUM_DISPLAY_PICTURES; count++)
            {
                uri = new Uri(iconImageBaseFolder + roles[count] + IMAGE_EXTENSION, UriKind.Relative);
                _iconPictures[count] = new BitmapImage(uri);
                ImgLoader.Source = _iconPictures[count];
            }
        }

        public static void loadImage(BitmapImage source)
        {
            ImgLoader.Dispatcher.Invoke(new ThreadStart(() => loadImageInvoke(source)));
        }

        private static void loadImageInvoke(BitmapImage source)
        {
            ImgLoader.Source = source;
        }

        private static void initPlayingCardPictures()
        {
            Uri uri;

            for (int count = 0; count < NUM_DISPLAY_PICTURES; count++)
            {
                uri = new Uri(playingCardBaseFolder + roles[count] + IMAGE_EXTENSION,UriKind.Relative);
                _playingCardPictures[count] = new BitmapImage(uri);
                ImgLoader.Source = _playingCardPictures[count];
            }

            uri = new Uri(playingCardBaseFolder + "back" + IMAGE_EXTENSION,UriKind.Relative);
            _playingCardPictures[8] = new BitmapImage(uri);
            ImgLoader.Source = _playingCardPictures[8];
        }

        public static void initConfiguration()
        {
            if (File.Exists(CONFIG_FILE))
            {
                loadFromFile();
            }
            else
            {
                FileInfo dir = new FileInfo(CONFIG_FILE);
                dir.Directory.Create();

                loadWithDefaults();
            }
        }
        private static void loadWithDefaults()
        {
            Username = Environment.UserName;
            portH = DEFAULT_HOST_PORT;
            lastJoinIP = DEFAULT_LAST_JOIN_IP;
            lastJoinPort = DEFAULT_JOIN_PORT;
            DisplayPictureIndex = DEFAULT_DISPLAY_PICTURE;
            LastDate = DateTime.Now;
        }
        public static void nextImg()
        {
            if (DisplayPictureIndex < NUM_DISPLAY_PICTURES - 1)
            {
                DisplayPictureIndex++;
            }
        }
        public static void prevImg()
        {
            if (DisplayPictureIndex > 0)
            {
                DisplayPictureIndex--;
            }  
        }
        public static bool hasNextImage()
        {
            return DisplayPictureIndex < NUM_DISPLAY_PICTURES - 1;
        }
        public static bool hasPrevImage()
        {
            return DisplayPictureIndex > 0;
        }
        private static void loadFromFile()
        {
            XmlTextReader xmlReader = new XmlTextReader(CONFIG_FILE);

            while(xmlReader.Read())
            {
                if(xmlReader.Name == "Config")
                {
                    Username = xmlReader.GetAttribute("Username");
                    DisplayPictureIndex = Convert.ToInt32(xmlReader.GetAttribute("Display"));
                    portH = Convert.ToInt32(xmlReader.GetAttribute("PortH"));

                    Timeout_Index = Convert.ToInt32(xmlReader.GetAttribute("TimeoutIndex"));

                    lastJoinPort = Convert.ToInt32(xmlReader.GetAttribute("PortC"));
                    lastJoinIP = xmlReader.GetAttribute("LastJoinIP");

                    DateTime.TryParseExact(xmlReader.GetAttribute("LastDate"),DATE_FORMAT, null, DateTimeStyles.None, out LastDate);
                }
            }

            xmlReader.Close();
        }
        public static void saveToFile()
        {
            StreamWriter file = new StreamWriter(CONFIG_FILE);
            XmlDocument doc = new XmlDocument();
            XmlElement el = (XmlElement)doc.AppendChild(doc.CreateElement("Config"));
                       
            el.SetAttribute("Username", Username);
            el.SetAttribute("Display", "" + DisplayPictureIndex);
            el.SetAttribute("PortH", "" + portH);

            el.SetAttribute("PortC", "" + lastJoinPort);
            el.SetAttribute("LastJoinIP", "" + lastJoinIP);

            el.SetAttribute("TimeoutIndex", "" + Timeout_Index);
            el.SetAttribute("LastDate", LastDate.ToString(DATE_FORMAT));

            file.Write(doc.OuterXml);
            file.Close();
        }
    }
}
