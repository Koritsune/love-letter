﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Restaurant
{
    public class Waiter
    {
        Socket handler;
        Thread workerThread;
        CommandQeue commandQeue;
        NetworkStatusManager networkStatusManager;

        int ID = -1;

        public int ServerID
        {
            get
            {
                return ID;
            }
        }

        int checkNetworkStatusInterval = Constants.INTERVAL_CHECK_NETWORK_STATUS;

        public int CheckNetworkStatusInterval
        {
            get
            {
                return checkNetworkStatusInterval;
            }
            set
            {
                checkNetworkStatusInterval = value;

                if (networkStatusManager != null)
                {
                    networkStatusManager.CheckNetworkStatusInterval = checkNetworkStatusInterval;
                }
            }
        }

        int pollNetworkStatusInterval = Constants.INTERVAL_POLL_NETWORK_STATUS;

        public int PollNetworkStatusInterval
        {
            get
            {
                return pollNetworkStatusInterval;
            }
            set
            {
                pollNetworkStatusInterval = value;

                if (networkStatusManager != null)
                {
                    networkStatusManager.PollNetworkStatusInterval = pollNetworkStatusInterval;
                }
            }
        }

        bool started = false;
        bool connected = false;

        public event ConnectionDeclinedHandler ConnectionDeclined;
        public delegate void ConnectionDeclinedHandler(object source,ConnectionDeclinedEvent e);
        public class ConnectionDeclinedEvent : EventArgs
        {
            public string DeclinedMessage;

            public ConnectionDeclinedEvent(string declinedMessage)
            {
                DeclinedMessage = declinedMessage;
            }
        }

        public event MessageReceivedEventHandler ACK_REQReceived;
        public event MessageReceivedEventHandler ACK_RSPReceived;
        public event MessageReceivedEventHandler QuitReceived;

        public event MessageReceivedEventHandler MessageReceived;
        public delegate void MessageReceivedEventHandler(object source, MessageReceivedEvent e);

        public event StatusChangeEventHandler StatusChanged;
        public delegate void StatusChangeEventHandler(object source, StatusChangeEvent e);
        public class StatusChangeEvent : EventArgs
        {
            public int ServerID;

            public StatusChangeEvent(int ID)
            {
                ServerID = ID;
            }
        }

        public event LostConnectionEventHandler LostConnection;
        public delegate void LostConnectionEventHandler(object source, LostConnectionEvent e);
        public class LostConnectionEvent : EventArgs
        {
            public int ServerID;

            public LostConnectionEvent(int ID)
            {
                ServerID = ID;
            }
        }

        public enum Network_Status
        {           
            LOST,
            BAD,
            WEAK,
            GOOD
        };

        private Network_Status network_status = Network_Status.GOOD;

        public Network_Status ConnectionQuality
        {
            get
            {
                return network_status;
            }
            set
            {
                if (network_status != value)
                {
                    network_status = value;

                    if (StatusChanged != null)
                    {
                        StatusChangeEvent e = new StatusChangeEvent(ID);
                        StatusChanged(this,e);
                    }

                    if (ConnectionQuality == Network_Status.LOST && LostConnection != null)
                    {
                        GracefulClose();

                        LostConnectionEvent e = new LostConnectionEvent(ID);
                        e.ServerID = ID;

                        LostConnection(this, e);
                    }
                }
            }
        }

        public class MessageReceivedEvent : EventArgs
        {
            public int ServerID;
            public string message;

            public MessageReceivedEvent(string _msg, int ID)
            {
                ServerID = ID;
                message = _msg;
            }            
        }

        public Waiter()
        {

        }

        public Waiter(Socket _handler, int _ID, int pollNetworkInterval, int checkNetworkInterval)
        {
            handler = _handler;
            ID = _ID;
        }

        public void InternalListen()
        {
            if (!started)
            {
                commandQeue = new CommandQeue(handler);

                workerThread = new Thread(() => ReadIncomingMessage());

                started = true;
                workerThread.Start();

                System.Threading.Thread.Sleep(100); //needs to pause here to give a chance for socket to begin lsitening

                networkStatusManager = new NetworkStatusManager(this, pollNetworkStatusInterval,checkNetworkStatusInterval);
            }
        }
        
        private void ReadIncomingMessage()
        {
            while (started)
            {
                string msg = commandQeue.next();

                JSONPackets.RestaurantPacket restaurant_packet = JSONPackets.generatePacketFromString(msg);

                if (MessageReceived != null && restaurant_packet.Message_Type == JSONPackets.MSGType.MSG)
                {
                    MessageReceivedEvent e = new MessageReceivedEvent(restaurant_packet.Message, ID);

                    MessageReceived(this, e);
                }
                else if (restaurant_packet.Message_Type == JSONPackets.MSGType.CONNECTED)
                {
                    connected = true;
                }
                else if (ACK_REQReceived != null && restaurant_packet.Message_Type == JSONPackets.MSGType.ACK_REQ)
                {
                    MessageReceivedEvent e = new MessageReceivedEvent(restaurant_packet.Message, ID);
                    ACK_REQReceived(this,e);
                }
                else if (ACK_RSPReceived != null && restaurant_packet.Message_Type == JSONPackets.MSGType.ACK_RSP)
                {
                    MessageReceivedEvent e = new MessageReceivedEvent(restaurant_packet.Message, ID);
                    ACK_RSPReceived(this, e);
                }
                else if (QuitReceived != null && restaurant_packet.Message_Type == JSONPackets.MSGType.QUIT)
                {
                    MessageReceivedEvent e = new MessageReceivedEvent(restaurant_packet.Message, ID);
                    QuitReceived(this, e);

                    if (ServerID == -1) //if this is an independant waiter
                    {
                        Stop();
                    }
                }
                else if (ConnectionDeclined != null && restaurant_packet.Message_Type == JSONPackets.MSGType.DECLINED)
                {
                    ConnectionDeclinedEvent e = new ConnectionDeclinedEvent(restaurant_packet.Message);
                    ConnectionDeclined(this, e);

                    Stop();
                }
            }
        }

        public void ConnectToServer(String Hostname, int Port)
        {
            try
            {
                IPHostEntry hostEntry = Dns.GetHostEntry(Hostname);

                IPAddress hostIP = null;

                foreach (IPAddress IPAddress in hostEntry.AddressList)
                {
                    if (IPAddress.AddressFamily == AddressFamily.InterNetwork)
                    {
                        hostIP = IPAddress;
                        break;
                    }
                }

                IPEndPoint localEndPoint = new IPEndPoint(hostIP, Port);

                // Create a TCP/IP socket.
                handler = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                InternalListen();

                handler.Connect(localEndPoint);
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                throw ex;
            }

            while (!connected) ;
        }

        public void Stop()
        {
            started = false;

            if (networkStatusManager != null)
            {
                networkStatusManager.Stop();
            }

            try
            {
                if (handler != null)
                {
                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }
            }
#pragma warning disable
            catch (Exception e)
            {
#pragma warning enable
            }
            connected = false;
        }

        public void SendMessage(String message)
        {
            string message_Packet = JSONPackets.generate_Packet_MSG(message);
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(message_Packet + Constants.DELIM_STREAM);

            try
            {
                // Begin sending the data to the remote device.
                handler.Send(byteData);
            }
#pragma warning disable
            catch (Exception e)
            {
#pragma warning enable
            }
        }

        public void SendConnected()
        {
            string message_Packet = JSONPackets.generate_Packet_CONNECTED();
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(message_Packet + Constants.DELIM_STREAM);

            try
            {
                // Begin sending the data to the remote device.
                handler.Send(byteData);
            }
#pragma warning disable
            catch (Exception e)
            {
#pragma warning enable
            }
        }

        public void SendACK_REQ()
        {
            string ACK_REQ_Packet = JSONPackets.generate_Packet_ACK_REQ();
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(ACK_REQ_Packet + Constants.DELIM_STREAM);

            try
            {
                // Begin sending the data to the remote device.
                handler.Send(byteData);
            }
#pragma warning disable
            catch (Exception e)
            {
#pragma warning enable
            }
        }

        public void SendACK_RSP()
        {
            string ACK_RSP_Packet = JSONPackets.generate_Packet_ACK_RSP();
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(ACK_RSP_Packet + Constants.DELIM_STREAM);

            try
            {
                // Begin sending the data to the remote device.
                handler.Send(byteData);
            }
#pragma warning disable
            catch (Exception e)
            {
#pragma warning enable
            }
        }

        public void SendDeclined(string reason)
        {
            string Declined_Packet = JSONPackets.generate_Packet_DECLINED(reason);
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(Declined_Packet + Constants.DELIM_STREAM);

            try
            {
                // Begin sending the data to the remote device.
                handler.Send(byteData);
            }
#pragma warning disable
            catch (Exception e)
            {
#pragma warning enable
            }
        }

        public void GracefulClose()
        {
            string Quit_Packet = JSONPackets.generate_Packet_QUIT();

            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(Quit_Packet + Constants.DELIM_STREAM);

            try
            {
                // Begin sending the data to the remote device.
                handler.Send(byteData);
            }
#pragma warning disable
            catch (Exception e)
            {
#pragma warning enable
            }

            Stop();
        }
    }
}
