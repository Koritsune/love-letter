﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Restaurant
{
    public static class JSONPackets
    {
        static JavaScriptSerializer JSONSerializer = new JavaScriptSerializer();

        static RestaurantPacket PACKET_ACK_REQ = new RestaurantPacket(MSGType.ACK_REQ);
        static RestaurantPacket PACKET_ACK_RSP = new RestaurantPacket(MSGType.ACK_RSP);
        static RestaurantPacket PACKET_CONNECTED = new RestaurantPacket(MSGType.CONNECTED);
        static RestaurantPacket PACKET_EMPTY = new RestaurantPacket(MSGType.EMPTY);
        static RestaurantPacket PACKET_QUIT = new RestaurantPacket(MSGType.QUIT);

        public enum MSGType
        {
            ACK_REQ,
            ACK_RSP,
            CONNECTED,
            DECLINED,
            EMPTY,
            MSG,
            QUIT
        }

        public class RestaurantPacket
        {
            public MSGType Message_Type { get; set; }
            public string Message { get; set; }

            public RestaurantPacket()
            {

            }

            public RestaurantPacket(MSGType _msg_Type, string _msg = "")
            {
                Message_Type = _msg_Type;
                Message = _msg;
            }
        }

        public static string generate_Packet_ACK_REQ()
        {
            return JSONSerializer.Serialize(PACKET_ACK_REQ);
        }

        public static string generate_Packet_ACK_RSP()
        {
            return JSONSerializer.Serialize(PACKET_ACK_RSP);
        }

        public static string generate_Packet_CONNECTED()
        {
            return JSONSerializer.Serialize(PACKET_CONNECTED);
        }

        public static string generate_Packet_DECLINED(string message)
        {
            RestaurantPacket declined = new RestaurantPacket(MSGType.DECLINED, message);

            return JSONSerializer.Serialize(declined);
        }

        public static string generate_Packet_EMPTY()
        {
            return JSONSerializer.Serialize(PACKET_EMPTY);
        }

        public static string generate_Packet_MSG(string msg)
        {
            RestaurantPacket message = new RestaurantPacket(MSGType.MSG, msg);

            return JSONSerializer.Serialize(message);
        }

        public static string generate_Packet_QUIT()
        {
            return JSONSerializer.Serialize(PACKET_QUIT);
        }

        public static RestaurantPacket generatePacketFromString(string msg)
        {
            RestaurantPacket restaurantPacket = JSONSerializer.Deserialize<RestaurantPacket>(msg);

            return restaurantPacket;
        }
    }
}
