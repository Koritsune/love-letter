﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Restaurant
{
    public class Server
    {
        Socket listener;
                
        bool started = false;
        volatile bool listening = false;

        public bool RequireApprovalForWaiters = false;

        int pollNetworkStatusInterval;

        public int PollNetworkStatusInterval
        {
            get
            {
                return pollNetworkStatusInterval;
            }
            set
            {
                pollNetworkStatusInterval = value;

                foreach (Waiter waiter in waiters)
                {
                    waiter.PollNetworkStatusInterval = pollNetworkStatusInterval;
                }
            }
        }

        int checkNetworkStatusInterval;

        public int CheckNetworkStatusInterval
        {
            get
            {
                return checkNetworkStatusInterval;
            }
            set
            {
                checkNetworkStatusInterval = value;

                foreach (Waiter waiter in waiters)
                {
                    waiter.CheckNetworkStatusInterval = checkNetworkStatusInterval;
                }
            }
        }

        public event WaiterLeftEventHandler WaiterLeft;
        public delegate void WaiterLeftEventHandler (object source, WaiterLeftEvent e);
        public class WaiterLeftEvent : EventArgs
        {
            public int ServerID;

            public WaiterLeftEvent(int ID)
            {
                ServerID = ID;
            }
        }

        public event NewWaiterAddedEventHandler NewWaiterAdded;
        public delegate void NewWaiterAddedEventHandler(object source, NewWaiterAddedEvent e);
        public class NewWaiterAddedEvent : EventArgs
        {
            public int ServerID;
            public Waiter WaiterAdded;

            public NewWaiterAddedEvent(int serverID, Waiter waiterAdded)
            {
                ServerID = serverID;
                WaiterAdded = waiterAdded;
            }
        }

        public event NewWaiterForApprovalHandler NewWaiterForApproval;
        public delegate void NewWaiterForApprovalHandler(object source, NewWaiterForApprovalEvent e);
        public class NewWaiterForApprovalEvent : EventArgs
        {
            public bool Approved = false;
            public string DeclinedMessage = "";
            public int ServerID;
            public Waiter waiter;

            public NewWaiterForApprovalEvent(Waiter _waiter)
            {
                ServerID = _waiter.ServerID;
                waiter = _waiter;
            }
        }

        public event WaiterLostConnectionEventHandler WaiterLostConnection;
        public delegate void WaiterLostConnectionEventHandler(object source, WaiterLostConnectionEvent e);
        public class WaiterLostConnectionEvent : EventArgs
        {
            public int ServerID;

            public WaiterLostConnectionEvent(int ID)
            {
                ServerID = ID;
            }
        }

        int nextID = 0;

        Thread workerThread;
        List<Waiter> waiters = new List<Waiter>();

        public event WaiterStatusChangeEventHandler WaiterStatusChanged;
        public delegate void WaiterStatusChangeEventHandler(object source, WaiterStatusChangeEvent e);
        public class WaiterStatusChangeEvent : EventArgs
        {
            public int ServerID;

            public WaiterStatusChangeEvent(int waiterID)
            {
                ServerID = waiterID;
            }
        }

        public event ServerMessageReceivedEventHandler ServerMessageReceived;
        public delegate void ServerMessageReceivedEventHandler(object source, ServerMessageReceivedEvent e);
        public class ServerMessageReceivedEvent : EventArgs
        {
            public string message;
            public int ServerID;

            public ServerMessageReceivedEvent(string _msg, int waiterID)
            {
                ServerID = waiterID;
                message = _msg;
            }

            
        }

        public Server(int _pollNetworkStatusInterval = Constants.INTERVAL_POLL_NETWORK_STATUS, int _checkNetworkStatusInterval = Constants.INTERVAL_CHECK_NETWORK_STATUS)
        {
            pollNetworkStatusInterval = _pollNetworkStatusInterval;
            checkNetworkStatusInterval = _checkNetworkStatusInterval;
        }

        public void AcceptConnections(String IPAddress, int Port, int backlogConnections = 100)
        {
            if (!started)
            {
                started = true;

                workerThread = new Thread(() => StartListening(IPAddress, Port,backlogConnections));
                workerThread.Start();

                while (!listening) ;
            }
        }

        public void CloseConnections()
        {
            started = false;

            if (listener != null)
            {
                listener.Close();
            }            

            foreach (Waiter waiter in waiters)
            {
                waiter.GracefulClose();
                waiter.Stop();
            }

            waiters.Clear();
        }

        private void StartListening(String IPAddress, int Port, int backlogConnections)
        {
            //Data buffer for incofming data
            byte[] bytes = new Byte[1024];

            IPEndPoint localEndPoint = new IPEndPoint(System.Net.IPAddress.Parse(IPAddress), Port);

            try
            {
                listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                listener.Bind(localEndPoint);
                listener.Listen(backlogConnections);

                listening = true;

                while (started)
                {
                    AcceptCallBack(listener.Accept());
                }
            }
            #pragma warning disable 
            catch (Exception e)
            {
                //Thread aborted
                return;
            }
            #pragma warning enable
        }

        private void AcceptCallBack(Socket handler)
        {
            Waiter waiter = new Waiter(handler,nextID,pollNetworkStatusInterval,checkNetworkStatusInterval);

            nextID++;

            waiter.MessageReceived += Waiter_MessageReceived;
            waiter.QuitReceived += waiter_QuitReceived;
            waiter.LostConnection += waiter_LostConnection;
            
            waiter.InternalListen();

            waiter.StatusChanged += waiter_StatusChanged;
            waiter.SendConnected();

            if (RequireApprovalForWaiters && NewWaiterForApproval != null)
            {
                NewWaiterForApprovalEvent e = new NewWaiterForApprovalEvent(waiter);

                NewWaiterForApproval(this, e);

                if (e.Approved)
                {
                    waiters.Add(waiter);

                    if (NewWaiterAdded != null)
                    {
                        NewWaiterAddedEvent e2 = new NewWaiterAddedEvent(waiter.ServerID,waiter);
                        NewWaiterAdded(this, e2);
                    }
                }
                else
                {
                    waiter.SendDeclined(e.DeclinedMessage);
                    waiter.Stop();
                }
            }
            else
            {
                waiters.Add(waiter);

                if (NewWaiterAdded != null)
                {
                    NewWaiterAddedEvent e2 = new NewWaiterAddedEvent(waiter.ServerID,waiter);
                    NewWaiterAdded(this, e2);
                }
            }
        }

        void waiter_LostConnection(object source, Waiter.LostConnectionEvent e)
        {
            Waiter waiter = (Waiter)source;

            if (WaiterLostConnection != null)
            {
                WaiterLostConnectionEvent e2 = new WaiterLostConnectionEvent(waiter.ServerID);
                WaiterLostConnection(this, e2);
            }

            waiter.Stop();
            waiters.Remove(waiter);
        }

        void waiter_QuitReceived(object source, Waiter.MessageReceivedEvent e)
        {
            Waiter waiter = (Waiter)source;

            if (WaiterLeft != null)
            {
                WaiterLeftEvent e2 = new WaiterLeftEvent(waiter.ServerID);
                WaiterLeft(this, e2);
            }

            waiter.Stop();
            waiters.Remove(waiter);
        }

        void waiter_StatusChanged(object source, Waiter.StatusChangeEvent e)
        {
            if (WaiterStatusChanged != null)
            {
                WaiterStatusChangeEvent e2 = new WaiterStatusChangeEvent(e.ServerID);
                WaiterStatusChanged(this, e2);
            }
        }

        private void Waiter_MessageReceived(object source, Waiter.MessageReceivedEvent e)
        {
            if (ServerMessageReceived != null)
            {
                ServerMessageReceivedEvent e2 = new ServerMessageReceivedEvent(e.message,e.ServerID);

                ServerMessageReceived(this, e2);
            }
        }

        public void SendMessage(String message)
        {
            foreach (Waiter waiter in waiters)
            {
                waiter.SendMessage(message);
            }
        }

        public void GracefulClose(int ServerID)
        {
            for (int count = 0; count < waiters.Count; count++)
            {
                if (waiters[count].ServerID == ServerID)
                {
                    waiters[count].GracefulClose();
                    waiters.RemoveAt(count);
                }
            }
        }
    }
}
