﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;

namespace Restaurant
{
    class NetworkStatusManager
    {
        Timer timerPollStatus;
        Timer timerCheckStatus;

        Waiter waiter;

        bool isAlive;

        int checkNetworkStatusInterval = Constants.INTERVAL_CHECK_NETWORK_STATUS;

        public int CheckNetworkStatusInterval
        {
            get
            {
                return checkNetworkStatusInterval;
            }
            set
            {
                checkNetworkStatusInterval = value;

                timerCheckStatus.Interval = checkNetworkStatusInterval;
            }
        }

        int pollNetworkStatusInterval = Constants.INTERVAL_POLL_NETWORK_STATUS;

        public int PollNetworkStatusInterval
        {
            get
            {
                return pollNetworkStatusInterval;
            }
            set
            {
                pollNetworkStatusInterval = value;

                timerPollStatus.Interval = pollNetworkStatusInterval;
            }
        }

        public NetworkStatusManager(Waiter _waiter, int _pollNetworkStatusInterval, int _checkNetworkStatusInterval)
        {
            waiter = _waiter;
            pollNetworkStatusInterval = _pollNetworkStatusInterval;
            checkNetworkStatusInterval = _checkNetworkStatusInterval;

            waiter.ACK_REQReceived += waiter_ACK_REQReceived;
            waiter.ACK_RSPReceived += waiter_ACK_RSPReceived;

            timerPollStatus = new Timer();
            timerPollStatus.Interval = pollNetworkStatusInterval;
            timerPollStatus.Elapsed += timerPollStatus_Elapsed;
            timerPollStatus.Start();

            timerCheckStatus = new Timer();
            timerCheckStatus.Interval = checkNetworkStatusInterval;
            timerCheckStatus.Elapsed += timerCheckStatus_Elapsed;
            timerCheckStatus.Start();
        }

        void waiter_ACK_RSPReceived(object source, Waiter.MessageReceivedEvent e)
        {
            isAlive = true;
        }

        void waiter_ACK_REQReceived(object source, Waiter.MessageReceivedEvent e)
        {
            waiter.SendACK_RSP();
        }

        void timerCheckStatus_Elapsed(object sender, ElapsedEventArgs e)
        { 
            if (isAlive && waiter.ConnectionQuality < Waiter.Network_Status.GOOD)
            {
                waiter.ConnectionQuality++;
            }
            else if (!isAlive && waiter.ConnectionQuality > Waiter.Network_Status.LOST)
            {
                waiter.ConnectionQuality--;
            }

            isAlive = false;
        }

        void timerPollStatus_Elapsed(object sender, ElapsedEventArgs e)
        {
            waiter.SendACK_REQ();
        }

        public void Stop()
        {
            timerCheckStatus.Stop();
            timerPollStatus.Stop();
        }
    }
}
