﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant
{
    public static class Constants
    {
        public const char DELIM_STREAM = (char)28;

        public const int INTERVAL_POLL_NETWORK_STATUS = 500;
        public const int INTERVAL_CHECK_NETWORK_STATUS = 1000;
    }
}
