﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace Restaurant
{
    public class CommandQeue
    {
        Queue<String> commands = new Queue<string>();
        String data = "";
        Socket handler;

        public CommandQeue(Socket handler_) {
            handler = handler_;
        }
        public String Dequeue() {
            return commands.Dequeue();
        }
        public void read() {
            try
            {
                
                byte[] bytes = new Byte[1024];
                
                while (true)
                {
                    bytes = new byte[1024];
                    int bytesRec = handler.Receive(bytes);
                    data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                    if (data.IndexOf(Constants.DELIM_STREAM) > -1)
                    {
                        break;
                    }
                }
                
                while (data.IndexOf(Constants.DELIM_STREAM) != -1)
                {
                    String command = data.Substring(0, data.IndexOf(Constants.DELIM_STREAM));
                    if (data.Length == data.IndexOf(Constants.DELIM_STREAM) + 1) //if stream delimeter is the last character in data
                    {
                        data = "";
                    }
                    else
                    {
                        data = data.Substring(command.Length + 1);
                    }

                    commands.Enqueue(command);
                }
            }
            #pragma warning disable 0168
            catch (SocketException ex)
            {
                // Socket Exception in commandQeue
            }
            catch (ObjectDisposedException ex)
            {
                // ObjectDisposed Exception in commandqueue
            }
            #pragma warning restore 0168
        }
        public bool isEmpty() {
            return commands.Count == 0;
        }
        public String next() {
            if (commands.Count == 0)
            {
                read();
            }
            if (commands.Count == 0)
            {
                return JSONPackets.generate_Packet_EMPTY();
            }
            else
            {
                String msgTemp = commands.Dequeue();

                if (msgTemp.Equals(""))
                {
                    return JSONPackets.generate_Packet_EMPTY();
                }

                return msgTemp;
            }
        }
    }
}
