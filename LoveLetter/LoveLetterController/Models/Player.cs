﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Media.Imaging;

namespace LoveLetterController.Models
{
    public class Player
    {
        public string Username { get; set; }
        public int DisplayIndex { get; set; }
        public DateTime LastDate { get; set; }

        public enum PlayerStatus
        {
            GOOD,
            WARNING,
            AI
        };

        private PlayerStatus status = PlayerStatus.GOOD;

        public PlayerStatus Connection_Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;

                if (ConnectionStatusChanged != null)
                {
                    ConnectionStatusChanged(this);
                }
            }
        }

        private bool ready;
        public bool Ready 
        {
            get
            {
                return ready;
            } 
            set 
            {
                ready = value;

                if (ReadyStatusChanged != null)
                {
                    ReadyStatusChanged();
                }
            } 
        }

        public event PlayerKickedEventHandler Removed;
        public delegate void PlayerKickedEventHandler(Player player);

        public event PlayerEventHandler ReadyStatusChanged;
        public delegate void PlayerEventHandler();

        public event PlayerKickedEventHandler ConnectionStatusChanged;

        [ScriptIgnore]
        public int ID {get;set;}
        [ScriptIgnore]
        public int DisplayPictureIndex
        {
            get
            {                
                return DisplayIndex;
            }
        }

        public Player()
        {
            ID = -1;
        }

        public Player(string username, int displayIndex, DateTime lastDate, bool ready)
        {
            Username = username;
            DisplayIndex = displayIndex;
            LastDate = lastDate;

            this.ready = ready; 
        }

        public void Remove()
        {
            if (Removed != null)
            {
                Removed(this);
            }
        }
    }
}
