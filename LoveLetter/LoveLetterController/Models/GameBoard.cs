﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoveLetterController.Models
{
    public class GameBoard
    {
        public static string Username;

        public List<Player> Players { get; set; }
        private static int timeout;

        public static int Timeout 
        { 
            get
            {
                return timeout;
            } 
            set
            {
                timeout = value;

                if(TimeoutChanged != null)
                {
                    TimeoutChanged();
                }
            }
        }


        public static event GameStartedEventHandler GameStartedEvent;
        public delegate void GameStartedEventHandler();

        private static bool gameStarted = false;
        public static bool GameStarted 
        { 
            get 
            { 
                return gameStarted; 
            } 
            set 
            {
                gameStarted = value;

                if (gameStarted && GameStartedEvent != null)
                {
                    GameStartedEvent();
                }
            } 
        }

        public static event TimeoutChangedHandler TimeoutChanged;
        public delegate void TimeoutChangedHandler();

        public event PlayerEventHandler NewPlayerAdded;
        public delegate void PlayerEventHandler(Player player);

        public event PlayerRemovedEventHandler PlayerRemoved;
        public delegate void PlayerRemovedEventHandler(object source, PlayerRemovedEvent e);
        public class PlayerRemovedEvent:EventArgs
        {
            public string Reason { get; set; }
            public Player RemovedPlayer { get; set; }

            public PlayerRemovedEvent(Player player, String reason)
            {
                Reason = reason;
                RemovedPlayer = player;
            }
        }

        public GameBoard()
        {
            Players = new List<Player>();
            GameStarted = false;
        }

        public bool doesUsernameExist(string username)
        {
            foreach (Player player in Players)
            {
                if (player.Username == username)
                {
                    return true;
                }
            }

            return false;
        }
        public bool canAddAnotherPlayer()
        {
            return Players.Count < GameConstants.MAX_PLAYERS;
        }
        public void addPlayer(Player newPlayer)
        {
            Players.Add(newPlayer);

            if (NewPlayerAdded != null)
            {
                NewPlayerAdded(newPlayer);
            }
        }
        public Player getPlayer(string username)
        {
            foreach (Player player in Players)
            {
                if (player.Username == username)
                {
                    return player;
                }
            }

            return null;
        }
        public void playerReady(string username)
        {
            Player player = getPlayer(username);

            player.Ready = true;
        }
        private bool areAllPlayersReady()
        {
            foreach (Player player in Players)
            {
                if (!player.Ready)
                {
                    return false;
                }
            }

            return true;
        }

        public void playerNotReady(string username)
        {
            Player player = getPlayer(username);

            player.Ready = false;
        }

        public class GameStartException : Exception
        {
            public string Reason { get; set; }

            public GameStartException(string reason)
            {
                Reason = reason;
            }
        }

        public bool canStartGame()
        {
            if (!areAllPlayersReady())
            {
                GameStartException gameStartException = new GameStartException(GameConstants.MESSAGE_PLAYERS_NOT_READY);

                throw gameStartException;
            }
            else if(Players.Count < GameConstants.MIN_PLAYERS) //program supposes that players cannot surpass maximum
            {
                GameStartException gameStartException = new GameStartException(GameConstants.MESSAGE_INSUFFICIENT_PLAYERS);

                throw gameStartException;
            }

            return true;
        }

        public string getUsernameFromID(int ID)
        {
            foreach (Player player in Players)
            {
                if (player.ID == ID)
                {
                    return player.Username;
                }
            }

            return null;
        }
        public void removePlayer(string username)
        {
            Player playerToRemove = getPlayer(username);

            if (!GameStarted)
            {
                Players.Remove(playerToRemove);

                if (PlayerRemoved != null)
                {
                    if (username == Username)
                    {
                        PlayerRemoved(this, new PlayerRemovedEvent(playerToRemove, GameConstants.MESSAGE_KICK));
                    }
                    else
                    {
                        PlayerRemoved(this, new PlayerRemovedEvent(playerToRemove, GameConstants.MESSAGE_LEFT));
                    }

                }
            }
            else
            {

            }
        }
        public void clearBoard()
        {
            Players.Clear();
        }
        public void updateDateForPlayer(string username, DateTime newDate)
        {
            Player playerToUpdate = getPlayer(username);

            playerToUpdate.LastDate = newDate;
        }
    }
}
