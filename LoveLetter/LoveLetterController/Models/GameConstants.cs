﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoveLetterController.Models
{
    public static class GameConstants
    {
        public const string VERSION = "0.1.1";

        public const int MIN_PLAYERS = 2;
        public const int MAX_PLAYERS = 4;

        public const string MESSAGE_KICK = "You have been kicked from the game by the host.";
        public const string MESSAGE_LEFT = "Player was removed from the game because they left.";

        public const string MESSAGE_PLAYERS_NOT_READY = "A game cannot be started because not all players are ready.\nMaybe you should threaten to devour their souls?";
        public const string MESSAGE_INSUFFICIENT_PLAYERS = "A game cannot be started because you have insufficient players.";

        public enum CARDS
        {
            Guard,
            Priest,
            Baron,
            Handmaid,
            Prince,
            King,
            Countess,
            Princess
        };
    }
}
