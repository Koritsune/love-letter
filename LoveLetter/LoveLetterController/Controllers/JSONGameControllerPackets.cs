﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using LoveLetterController.Models;

namespace LoveLetterController.Controllers
{
    public static class JSONGameControllerPackets
    {
        static JavaScriptSerializer JSONSerializer = new JavaScriptSerializer();

        public enum GameMessageType
        {
            READY,
            NOT_READY,
            CARD,
            NEW_DATE,
            TIMEOUT,
            GAME_START
        }

        public class TimeoutPacket
        {
            public int Timeout { get; set; }

            public TimeoutPacket()
            {
                
            }

            public TimeoutPacket(int timeout)
            {
                Timeout = timeout;
            }
        }

        public class DatePacket
        {
            public DateTime LastDate { get; set; }

            public DatePacket()
            {

            }

            public DatePacket(DateTime lastDate)
            {
                LastDate = lastDate;
            }
        }

        public class GamePacket
        {
            public GameMessageType GameMessage_Type { get; set; }
            public string Player { get; set; }
            public string Content { get; set; }

            public GamePacket()
            {

            }

            public GamePacket(GameMessageType type, string content = "")
            {
                GameMessage_Type = type;
                Player = GameBoard.Username;
                Content = content;
            }
        }

        public static GamePacket ReadyPacket = new GamePacket(GameMessageType.READY);
        public static GamePacket NotReadyPacket = new GamePacket(GameMessageType.NOT_READY);
        public static GamePacket GameStartPacket = new GamePacket(GameMessageType.GAME_START);

        public static GamePacket generateReadyPacket()
        {
            return ReadyPacket;
        }

        public static GamePacket generateNotReadyPacket()
        {
            return NotReadyPacket;
        }

        public static GamePacket generateGamePacketFromString(string content)
        {
            return JSONSerializer.Deserialize<GamePacket>(content);
        }

        public static DatePacket generateDatePacketFromString(string content)
        {
            return JSONSerializer.Deserialize<DatePacket>(content);
        }

        public static GamePacket generateDatePacket(DateTime newDate)
        {
            DatePacket datePacket = new DatePacket(newDate);
            string datePacketString = JSONSerializer.Serialize(datePacket);

            GamePacket gamePacket = new GamePacket(GameMessageType.NEW_DATE, datePacketString);
            return gamePacket;
        }

        public static TimeoutPacket generateTimeoutPacketFromString(string content)
        {
            return JSONSerializer.Deserialize<TimeoutPacket>(content);
        }

        public static GamePacket generateTimeoutPacket(int timeout)
        {
            TimeoutPacket timeoutPacket = new TimeoutPacket(timeout);
            string timeoutPacketString = JSONSerializer.Serialize(timeoutPacket);

            GamePacket gamepacket = new GamePacket(GameMessageType.TIMEOUT, timeoutPacketString);
            return gamepacket;
        }
    }
}
