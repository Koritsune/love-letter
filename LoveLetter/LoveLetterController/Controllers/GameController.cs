﻿using LoveLetterController.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoveLetterController.Controllers
{
    public class GameController
    {
        //static NetworkController networkController;

        private static GameBoard gameBoard;
        public static GameBoard GameBoard
        {
            get
            {
                return gameBoard;
            }
            set
            {
                gameBoard = value;

                if (GameBoardCreated != null)
                {
                    GameBoardCreated();
                }
            }
        }

        public static event GamePacketSendRequestHandler GamePacketSendRequest;
        public delegate void GamePacketSendRequestHandler(JSONGameControllerPackets.GamePacket gamePacket);

        public static event GameBoardCreatedHandler GameBoardCreated;
        public delegate void GameBoardCreatedHandler();

        public static void init()
        {
            gameBoard = new GameBoard();
        }

        public static void processGamePacket(JSONGameControllerPackets.GamePacket gamePacket)
        {

            if (gamePacket.GameMessage_Type == JSONGameControllerPackets.GameMessageType.CARD)
            {

            }
            else if (gamePacket.GameMessage_Type == JSONGameControllerPackets.GameMessageType.READY)
            {
                GameBoard.playerReady(gamePacket.Player);
            }
            else if (gamePacket.GameMessage_Type == JSONGameControllerPackets.GameMessageType.NOT_READY)
            {
                GameBoard.playerNotReady(gamePacket.Player);
            }
            else if (gamePacket.GameMessage_Type == JSONGameControllerPackets.GameMessageType.NEW_DATE)
            {
                JSONGameControllerPackets.DatePacket datePacket = JSONGameControllerPackets.generateDatePacketFromString(gamePacket.Content);
                
                gameBoard.updateDateForPlayer(gamePacket.Player, datePacket.LastDate);
            }
            else if (gamePacket.GameMessage_Type == JSONGameControllerPackets.GameMessageType.TIMEOUT) 
            {
                JSONGameControllerPackets.TimeoutPacket timeoutPacket = JSONGameControllerPackets.generateTimeoutPacketFromString(gamePacket.Content);

                GameBoard.Timeout = timeoutPacket.Timeout;
            }
            else if (gamePacket.GameMessage_Type == JSONGameControllerPackets.GameMessageType.GAME_START)
            {
                GameBoard.GameStarted = true;
            }
        }

        public static void sendReadyPacket()
        {
            JSONGameControllerPackets.GamePacket readyPacket = JSONGameControllerPackets.generateReadyPacket();

            if (GamePacketSendRequest != null)
            {
                GamePacketSendRequest(readyPacket);
            }

        }

        public static void sendNotReadyPacket()
        {
            JSONGameControllerPackets.GamePacket notReadyPacket = JSONGameControllerPackets.generateNotReadyPacket();

            if (GamePacketSendRequest != null)
            {
                GamePacketSendRequest(notReadyPacket);
            }

        }

        public static void sendNewDatePacket(DateTime newDate)
        {
            JSONGameControllerPackets.GamePacket newDatePacket = JSONGameControllerPackets.generateDatePacket(newDate);

            if (GamePacketSendRequest != null)
            {
                GamePacketSendRequest(newDatePacket);
            }
        }

        public static void sendTimeoutPacket(int timeout)
        {
            JSONGameControllerPackets.GamePacket timeoutPacket = JSONGameControllerPackets.generateTimeoutPacket(timeout);

            if (GamePacketSendRequest != null)
            {
                GamePacketSendRequest(timeoutPacket);
            }
        }

        public static void sendGameStartPacket()
        {
            JSONGameControllerPackets.GamePacket gameStartPacket = JSONGameControllerPackets.GameStartPacket;

            if (GamePacketSendRequest != null)
            {
                GamePacketSendRequest(gameStartPacket);
            }
        }

        public static bool canGameStart()
        {
            return gameBoard.canStartGame();
        }
    }
}
